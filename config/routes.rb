Rails.application.routes.draw do
  
  get 'password_resets/new'

  get 'password_resets/edit'

  get 'sessions/new'

  get 'users/new'

  root 'static_pages#test'
  
  get 'static_pages/home'

  get 'static_pages/help'
  
  get   '/home'  , to:'static_pages#home'
  get   '/music' , to:'static_pages#music'
  get   '/help'  , to:'static_pages#help'
  get   '/signup', to: 'users#new'
  get    '/login',   to: 'sessions#new'
  post   '/login',   to: 'sessions#create'
  delete '/logout',  to: 'sessions#destroy'
  resources :users
  resources :account_activations, only: [:edit]
  resources :password_resets,     only: [:new, :create, :edit, :update]

end
